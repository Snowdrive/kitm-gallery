<?php include '_partials/header.view.php'; ?>
<?php include '_partials/bootstrap.include.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forbidden</title>
</head>
<body>
    <div class="d-flex flex-column justify-content-center align-items-center align-self-center mt-5 ">
        <div class="mt-5">
            <p class="h1">Jūs neturite prieigos prie šio puslapio.</p>
        </div>
        <div class="mt-5">
            <p class="h2">Klaida 403: Forbidden</p>
        </div>
    </div>
</body>
</html>