<?php include '_partials/header.view.php'; ?>
<?php include '_partials/bootstrap.include.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Galerija</title>
    <style>
    .card-img-top {
    width: 20rem;
    height: 70%;
    object-fit: cover;
    }
    </style>
</head>
<body>
    <?php 
    $gallery_id = $_GET["id"];

    if ($gallery_id == null){
        header('location:../forbidden.php');
        die();
    }

    include 'functions/getImagesInGallery.php';

    $db = Database::connect();

    try{
        $stmt=$db->prepare("SELECT * from `galleries` WHERE id = ?"); //fetch info abt gallery from $_GET
        $stmt->execute(["$gallery_id"]);
    }
    catch(PDOException $e){
        echo $stmt . "<br>" . $e->getMessage();
    }
    
    $galleryInfo = $stmt->fetch(PDO::FETCH_ASSOC);

    if ($galleryInfo["description"] == ""){
        $galleryInfo["description"] = "Aprašymas nepateikas.";
    }

    $belongsToIDFetch = $galleryInfo["belongs_to"]; // fetch name and profile pic of user ID that the gallery belongs to

    try{
        $stmt=$db->prepare("SELECT * from `users` WHERE google_id = ?");
        $stmt->execute(["$belongsToIDFetch"]);
    }
    catch(PDOException $e){
        echo $stmt . "<br>" . $e->getMessage();
    }

    $belongsTo = $stmt->fetch(PDO::FETCH_ASSOC);

    $galleryInfo["belongs_to_displayname"] = $belongsTo["display_name"];
    $galleryInfo["belongs_to_profilepic"] = $belongsTo["profile_image"];

    include $_SERVER['DOCUMENT_ROOT'].'/_partials/galleryModals.view.php';
    ?>


    <?php
        if($galleryInfo["belongs_to"] == $_SESSION["id"]){ //if owns gallery, show edit options
            echo    '<div class="mt-3 mb-3 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-evenly align-items-center flex-wrap">';
            echo        '<button class="btn btn-success btn-lg text-truncate mt-1 mb-1" data-bs-toggle="modal" data-bs-target="#uploadImageModal">Įkelti nuotrauką</button>';
            echo        '<button class="btn btn-primary btn-lg text-truncate mt-1 mb-1" data-bs-toggle="modal" data-bs-target="#editGalleryNameModal">Redaguoti pavadinimą</button>';
            echo        '<button class="btn btn-primary btn-lg text-truncate mt-1 mb-1" data-bs-toggle="modal" data-bs-target="#editGalleryDescriptionModal">Redaguoti aprašymą</button>';
            echo        '<button class="btn btn-danger btn-lg text-truncate mt-1 mb-1" data-bs-toggle="modal" data-bs-target="#deleteGalleryModal">Ištrinti galeriją</button>';
            echo    '</div>';
        }
    ?>

    <div class="mt-3 mb-3 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-between align-items-center overflow-hidden">
        <div class="ms-3" style="max-width:50%;">
            <div class="text-center mt-3 mb-3">
                <p class="h1"><?php echo $galleryInfo["name"] ?></p>
            </div>
            <div class="text-center mt-3 mb-3">
                <p class="h4"><i><?php echo $galleryInfo["description"] ?></i></p>
            </div>
            <div class="text-center mt-3 mb-3">
                <p class="h6">Sukurta: <?php echo $galleryInfo["date_created"] ?></p>
            </div>
        </div>
        <div class="me-3">
            <div class="text-center">
                <img src="<?php echo $galleryInfo["belongs_to_profilepic"]?>" class="rounded-circle img-fluid" style="max-width: 50%;">
            </div>
            <div class="text-center">
                <p class="h5 mt-2"><?php echo "<a href='viewprofile.php?id=".$galleryInfo["belongs_to"]."'style=text-decoration:none;>"."Autorius: ".$galleryInfo["belongs_to_displayname"]."</a>"; ?></p>
            </div>
        </div>
    </div>

    <div class="mt-3 mb-3 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-evenly align-items-center flex-wrap">
        <?php 
            foreach($imagesInfo as $imagesInfo1){
                echo '<div class="card ms-2 me-2 mt-3 mb-3 text-wrap" style="width: 20rem; height: 20rem;">';
                echo '<img src="'.$imagesInfo1["location"].'" class="card-img-top">';
                echo '<div class="card-body">';
                echo '<a href=viewimage.php?id='.$imagesInfo1["id"].' class="stretched-link text-decoration-none">';
                echo '<h5 class="card-title">'.$imagesInfo1["name"].'</h5>';
                echo '</a>';
                if ($imagesInfo1["description"] == ""){
                    echo '<p class="card-text text-truncate">Aprašymas nepateikas.</p>';  
                } else {
                    echo '<p class="card-text text-truncate">'.$imagesInfo1["description"].'</p>'; 
                }
                echo '</div>';
                echo '</div>';
            }
        ?>
    </div>

    <?php include '_partials/footer.view.php'; ?>
    <?php $_GET = array() ?>
</body>
</html>