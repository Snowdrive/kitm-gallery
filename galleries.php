<?php include '_partials/header.view.php'; ?>
<?php include '_partials/bootstrap.include.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Galerijos</title>
</head>
<body>

    <div class="mt-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center">
        <div class="h4">Mano Galerijos</div>
    </div>

    <div class="mt-2 mb-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center flex-wrap">
    <?php 

    if($_SESSION["id"] !== null){

        include 'functions/getCurrentUserGalleries.php'; //current logged in users galleries 

        foreach($galleryInfo as $galleryInfo1){
            echo '<div class="card ms-2 me-2 mt-1 mb-1 text-wrap" style="width: 18rem; height: 13rem;">';
            echo '<div class="card-body">';
            echo '<h5 class="card-title">'.$galleryInfo1["name"].'</h5>';
            if ($galleryInfo1["description"] == ""){
                echo '<p class="card-text text-wrap" style="height:4.5rem;">Aprašymas nepateiktas.</p>';  
            } else {
                echo '<p class="card-text text-wrap overflow-hidden" style="height:4.5rem;">'.$galleryInfo1["description"].'</p>'; 
            }
            echo '<a href="viewgallery.php?id='.$galleryInfo1["id"].'" class="btn btn-primary mt-3 stretched-link">Naršyti</a>';
            echo '</div>';
            echo '</div>';   
        }
    } else{
        echo '<h4 class="pt-5 pb-5">Jūs neesate prisijungę.</h1>';
    }
    ?>
    </div>

    <div class="mt-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center">
        <div class="h4">Kitos Galerijos</div>
    </div>

    <div class="mt-2 mb-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center flex-wrap">
    <?php 
    if($_SESSION["id"] !== null){

        include 'functions/getOthersGalleries.php'; //all other galleries 

        foreach($othersGalleryInfo as $othersGalleryInfo1){
            echo '<div class="card ms-2 me-2 mt-1 mb-1 text-wrap" style="width: 18rem; height: 13rem;">';
            echo '<div class="card-body">';
            echo '<h5 class="card-title">'.$othersGalleryInfo1["name"].'</h5>';
            if ($othersGalleryInfo1["description"] == ""){
                echo '<p class="card-text text-wrap" style="height:4.5rem;">Aprašymas nepateikas.</p>';  
            } else {
                echo '<p class="card-text text-wrap overflow-hidden" style="height:4.5rem;">'.$othersGalleryInfo1["description"].'</p>'; 
            }
            echo '<a href="viewgallery.php?id='.$othersGalleryInfo1["id"].'" class="btn btn-primary mt-3 stretched-link">Naršyti</a>';
            echo '</div>';
            echo '</div>';    
        }
    } else {

        include 'functions/getAllGalleries.php'; //ALL galleries if not logged in

        foreach($allGalleryInfo as $allGalleryInfo1){
            echo '<div class="card ms-2 me-2 mt-1 mb-1 text-wrap" style="width: 18rem; height: 13rem;">';
            echo '<div class="card-body">';
            echo '<h5 class="card-title">'.$allGalleryInfo1["name"].'</h5>';
            if ($allGalleryInfo1["description"] == ""){
                echo '<p class="card-text text-wrap" style="height:4.5rem;">Aprašymas nepateikas.</p>';  
            } else {
                echo '<p class="card-text text-wrap overflow-hidden" style="height:4.5rem;">'.$allGalleryInfo1["description"].'</p>'; 
            }
            echo '<a href="viewgallery.php?id='.$allGalleryInfo1["id"].'" class="btn btn-primary mt-3 stretched-link">Naršyti</a>';
            echo '</div>';
            echo '</div>';    
        }
    }    
    ?>
    </div>

    <?php include '_partials/footer.view.php'; ?>

</body>
</html>