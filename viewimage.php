<?php include '_partials/header.view.php'; ?>
<?php include '_partials/bootstrap.include.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuotrauka</title>
</head>
<body>
    <?php
        $image_id = $_GET["id"];

        if ($image_id == ""){
            header('location:../forbidden.php');
            die();
        }
    
        $db = Database::connect();

        try{
            $stmt = $db->prepare("SELECT name, description, location, date_added, belongs_to, id FROM images WHERE id = ?");
            $stmt->execute([$image_id]);
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    
            $imageInfo = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($imageInfo["description"] == ""){
                $imageInfo["description"] = "Aprašymas nepateikas.";
            }
    ?>

    <div class="mt-2 mb-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center overflow-hidden">
        <div class="ms-3" style="max-width:50%;">
            <div class="text-center mt-2 mb-2">
                <p class="h1"><?php echo $imageInfo["name"] ?></p>
            </div>
            <div class="text-center mt-2 mb-2">
                <p class="h4"><i><?php echo $imageInfo["description"] ?></i></p>
            </div>
            <div class="text-center mt-2 mb-2">
                <p class="h6">Įkelta: <?php echo $imageInfo["date_added"] ?></p>
            </div>
        </div>
    </div>
    
    <div class="mt-2 mb-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center overflow-hidden">
        <div>
            <img src="<?php echo $imageInfo["location"]; ?>"" class="img-fluid" style="max-height: 60vh; max-width: 90vw;">
        </div>
    </div>

    <?php
        if($_SESSION["id"] == null){
            return;
        } else {
            try{
                $stmt = $db->prepare('SELECT belongs_to FROM galleries WHERE id = ?');
                $stmt->execute([$imageInfo["belongs_to"]]);
            }
            catch(PDOException $e){
                echo $e->getMessage();
            }

            $imageOwner = $stmt->fetch(PDO::FETCH_ASSOC);

            if($imageOwner["belongs_to"] == $_SESSION["id"]){

                include '_partials/imageModals.view.php';

                echo    '<div class="mt-2 mb-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-evenly align-items-center flex-wrap">';
                echo        '<button class="btn btn-primary btn-lg text-truncate mt-1 mb-1" data-bs-toggle="modal" data-bs-target="#editImageNameModal">Redaguoti pavadinimą</button>';
                echo        '<button class="btn btn-primary btn-lg text-truncate mt-1 mb-1" data-bs-toggle="modal" data-bs-target="#editImageDescriptionModal">Redaguoti aprašymą</button>';
                echo        '<button class="btn btn-danger btn-lg text-truncate mt-1 mb-1" data-bs-toggle="modal" data-bs-target="#deleteImageModal">Ištrinti nuotrauką</button>';
                echo    '</div>';
            }
        }
    ?>
        
</body>
</html>