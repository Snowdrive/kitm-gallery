
            <div class="modal fade" id="editImageNameModal" tabindex="-1" aria-labelledby="editImageNameModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form action="functions/editImageName.php" method="post">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editImageNameModalLabel">Redaguojama nuotrauka</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                 <label for="image-name" class="col-form-label">Įveskite naują nuotraukos pavadinimą</label>
                                 <input type="text" class="form-control" id="imagename" name="imagename" placeholder="Įveskite naują nuotraukos pavadinimą..." required=true>
                                 <input type="hidden" name="imageID" value="<?php echo $imageInfo["id"]?>" />
                            </div>  
                        </div>
                        <div class="modal-footer" style="margin-bottom: -15px;">
                            <button type="button" class="btn btn-secondary" style="margin-right:auto;" data-bs-dismiss="modal">Atšaukti</button>
                            <input type="submit" class="btn btn-primary" value="Atnaujinti">
                        </div>
                    </form>
                </div>
             </div>
        </div>

        <div class="modal fade" id="editImageDescriptionModal" tabindex="-1" aria-labelledby="editImageDescriptionModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form action="functions/editImageDescription.php" method="post">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editImageDescriptionModalLabel">Redaguojama nuotrauka</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                 <label for="image-name" class="col-form-label">Įveskite naują nuotraukos aprašymą</label>
                                 <input type="text" class="form-control" id="imagedescription" name="imagedescription" placeholder="Įveskite naują nuotraukos pavadinimą... (neprivalomas)">
                                 <input type="hidden" name="imageID" value="<?php echo $imageInfo["id"]?>" />
                            </div>  
                        </div>
                        <div class="modal-footer" style="margin-bottom: -15px;">
                            <button type="button" class="btn btn-secondary" style="margin-right:auto;" data-bs-dismiss="modal">Atšaukti</button>
                            <input type="submit" class="btn btn-primary" value="Atnaujinti">
                        </div>
                    </form>
                </div>
             </div>
        </div>

        <div class="modal fade" id="deleteImageModal" tabindex="-1" aria-labelledby="deleteImageModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form action="functions/deleteImage.php" method="post">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteImageModalLabel">Ištrinti nuotrauką?</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <h5><b>Ar tikrai norite ištrinti <?php echo $imageInfo["name"] ?>?</b></h5>
                            <p class="mt-3 mb-3">Nuotraukų ištrinimas yra amžinas.</p>
                            <div>
                            <input type="hidden" name="imageID" value="<?php echo $imageInfo["id"]?>" />
                            <input type="hidden" name="imageOwner" value="<?php echo $imageInfo["belongs_to"]?>" />
                            </div>  
                        </div>
                        <div class="modal-footer" style="margin-bottom: -15px;">
                            <button type="button" class="btn btn-secondary" style="margin-right:auto;" data-bs-dismiss="modal">Atšaukti</button>
                            <input type="submit" class="btn btn-danger" value="Ištrinti">
                        </div>
                    </form>
                </div>
             </div>
        </div>