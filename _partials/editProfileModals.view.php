<div class="modal fade" id="editDisplayNameModal" tabindex="-1" aria-labelledby="editDisplayNameModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form action="functions/editDisplayName.php" method="post">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editDisplayNameModalLabel">Redaguojamas profilis</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                 <label for="gallery-name" class="col-form-label">Keisti vardą</label>
                                 <input type="text" class="form-control" id="displayname" name="displayname" placeholder="Įveskite naują vardą..." required=true>
                            </div>  
                        </div>
                        <div class="modal-footer" style="margin-bottom: -15px;">
                            <button type="button" class="btn btn-secondary" style="margin-right:auto;" data-bs-dismiss="modal">Atšaukti</button>
                            <input type="submit" class="btn btn-primary" value="Atnaujinti">
                        </div>
                    </form>
                </div>
             </div>
        </div>
        <div class="modal fade" id="createGalleryModal" tabindex="-1" aria-labelledby="createGalleryModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form action="functions/addGallery.php" method="post">
                        <div class="modal-header">
                            <h5 class="modal-title" id="createGalleryModalLabel">Sukurti naują galeriją</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                 <label for="gallery-name" class="col-form-label">Galerijos pavadinimas</label>
                                 <input type="text" class="form-control" id="galleryname" name="galleryname" placeholder="Įveskite galerijos pavadinimą..." required=true>
                                 <label for="gallery-description" class="col-form-label">Galerijos aprašymas</label>
                                 <input type="text" class="form-control" id="gallerydescription" name="gallerydescription" placeholder="Įveskite galerijos aprašymą... (neprivaloma)">
                            </div>  
                        </div>
                        <div class="modal-footer" style="margin-bottom: -15px;">
                            <button type="button" class="btn btn-secondary" style="margin-right:auto;" data-bs-dismiss="modal">Atšaukti</button>
                            <input type="submit" class="btn btn-primary" value="Sukurti">
                        </div>
                    </form>
                </div>
             </div>
        </div>