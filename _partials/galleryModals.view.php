        <div class="modal fade" id="uploadImageModal" tabindex="-1" aria-labelledby="uploadImageModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form action="functions/uploadImage.php" method="post" enctype="multipart/form-data">
                        <div class="modal-header">
                            <h5 class="modal-title" id="uploadImageModalLabel">Keliama nauja nuotrauka</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                 <label class="custom-file-label" for="imageToUpload">Pasirinkite nuotrauką</label>
                                 <input type="file" class="custom-file-input" name="imageToUpload" id="imageToUpload" required=true>
                                 <br>
                                 <label for="gallery-name" class="col-form-label">Įveskite nuotraukos pavadinimą</label>
                                 <input type="text" class="form-control" id="imagename" name="imagename" placeholder="Įveskite nuotraukos pavadinimą..." required=true>
                                 <label for="gallery-name" class="col-form-label">Įveskite nuotraukos aprašymą</label>
                                 <input type="text" class="form-control" id="imagedescription" name="imagedescription" placeholder="Įveskite nuotraukos aprašymą... (neprivaloma)">
                                 <input type="hidden" name="galleryID" value="<?php echo $galleryInfo["id"]?>" />
                                 <input type="hidden" name="galleryOwner" value="<?php echo $galleryInfo["belongs_to"]?>" />
                            </div>  
                        </div>
                        <div class="modal-footer" style="margin-bottom: -15px;">
                            <button type="button" class="btn btn-secondary" style="margin-right:auto;" data-bs-dismiss="modal">Atšaukti</button>
                            <input type="submit" class="btn btn-primary" value="Įkelti">
                        </div>
                    </form>
                </div>
             </div>
        </div>
        
        <div class="modal fade" id="editGalleryNameModal" tabindex="-1" aria-labelledby="editGalleryNameModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form action="functions/editGalleryName.php" method="post">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editGalleryNameModalLabel">Redaguojama galerija</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                 <label for="gallery-name" class="col-form-label">Įveskite naują galerijos pavadinimą</label>
                                 <input type="text" class="form-control" id="galleryname" name="galleryname" placeholder="Įveskite naują galerijos pavadinimą..." required=true>
                                 <input type="hidden" name="galleryID" value="<?php echo $galleryInfo["id"]?>" />
                                 <input type="hidden" name="galleryOwner" value="<?php echo $galleryInfo["belongs_to"]?>" />
                            </div>  
                        </div>
                        <div class="modal-footer" style="margin-bottom: -15px;">
                            <button type="button" class="btn btn-secondary" style="margin-right:auto;" data-bs-dismiss="modal">Atšaukti</button>
                            <input type="submit" class="btn btn-primary" value="Atnaujinti">
                        </div>
                    </form>
                </div>
             </div>
        </div>

        <div class="modal fade" id="editGalleryDescriptionModal" tabindex="-1" aria-labelledby="editGalleryDescriptionModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form action="functions/editGalleryDescription.php" method="post">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editGalleryDescriptionModalLabel">Redaguojama galerija</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-3">
                                 <label for="gallery-name" class="col-form-label">Įveskite naują galerijos aprašymą</label>
                                 <input type="text" class="form-control" id="gallerydescription" name="gallerydescription" placeholder="Įveskite naują galerijos aprašymą... (neprivalomas)">
                                 <input type="hidden" name="galleryID" value="<?php echo $galleryInfo["id"]?>" />
                                 <input type="hidden" name="galleryOwner" value="<?php echo $galleryInfo["belongs_to"]?>" />
                            </div>  
                        </div>
                        <div class="modal-footer" style="margin-bottom: -15px;">
                            <button type="button" class="btn btn-secondary" style="margin-right:auto;" data-bs-dismiss="modal">Atšaukti</button>
                            <input type="submit" class="btn btn-primary" value="Atnaujinti">
                        </div>
                    </form>
                </div>
             </div>
        </div>

        <div class="modal fade" id="deleteGalleryModal" tabindex="-1" aria-labelledby="deleteGalleryModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <form action="functions/deleteGallery.php" method="post">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteGalleryModalLabel">Ištrinti galeriją?</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <h5><b>Ar tikrai norite ištrinti <?php echo $galleryInfo["name"] ?>?</b></h5>
                            <p class="mt-3 mb-3">Galerijos ištrynimas yra amžinas, ir tai neištrins nuotraukų, esančių galerijoje. Tai privalote padaryti rankiniu būdu.</p>
                            <div>
                            <input class="form-check-input" type="checkbox" value="" id="checkForDelete" required=true>
                            <label class="form-check-label" for="checkForDelete">
                                 Pažymėkite, kad ištrintumėte galeriją
                            </label>
                            <input type="hidden" name="galleryID" value="<?php echo $galleryInfo["id"]?>" />
                            <input type="hidden" name="galleryOwner" value="<?php echo $galleryInfo["belongs_to"]?>" />
                            </div>  
                        </div>
                        <div class="modal-footer" style="margin-bottom: -15px;">
                            <button type="button" class="btn btn-secondary" style="margin-right:auto;" data-bs-dismiss="modal">Atšaukti</button>
                            <input type="submit" class="btn btn-danger" value="Ištrinti">
                        </div>
                    </form>
                </div>
             </div>
        </div>