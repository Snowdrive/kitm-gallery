<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-magic.php';

    $db = Database::connect();
    $googleID = $_SESSION['id'];
    $gallery_name = $_POST["galleryname"];
    $gallery_description = $_POST["gallerydescription"];

    function addGallery(){
        global $db;
        global $googleID;
        global $gallery_name;
        global $gallery_description;

        if($googleID === null){ // block from accessing if logged out
            header('location:../forbidden.php');
            return;
        }

        if($gallery_name == ""){ // block from adding if no gallery name
            header('location:../forbidden.php');
            return;
        }

        try{
            $insertGallery = "INSERT INTO galleries (belongs_to, name, description) VALUES (?,?,?)";
            $db->prepare($insertGallery)->execute([$googleID, $gallery_name, $gallery_description]);
        }
        catch(PDOException $e){
            echo $insertGallery . "<br>" . $e->getMessage();
        }

    }

    addGallery();
    $_POST = array();
    header('location:../myprofile.php');

?>