<?php

//index.php

//Include Configuration File
include $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-config.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/functions/addUser.php';

$login_button = '';

if(isset($_GET["code"]))
{
 $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

 if(!isset($token['error']))
 {
  $google_client->setAccessToken($token['access_token']);

  $_SESSION['access_token'] = $token['access_token'];

  $google_service = new Google_Service_Oauth2($google_client);

  $data = $google_service->userinfo->get();

  if(!empty($data['given_name']))
  {
   $_SESSION['first_name'] = $data['given_name'];
  }

  if(!empty($data['family_name']))
  {
   $_SESSION['last_name'] = $data['family_name'];
  }

  if(!empty($data['email']))
  {
   $_SESSION['email_address'] = $data['email'];
  }

  if(!empty($data['picture']))
  {
   $_SESSION['image'] = $data['picture'];
  }

  $_SESSION["image2"] = str_replace("s96-c", "s256-c", $_SESSION["image"]);

  if(!empty($data['id']))
  {
   $_SESSION['id'] = $data['id'];
  }

  $_SESSION["defaultDisplayName"] = $_SESSION['first_name']." ".$_SESSION['last_name'];
 }
}
if(!isset($_SESSION['access_token']))
{
 $login_button = '<a href="'.$google_client->createAuthUrl().'"><img src="assets/signin.png" class="rounded" style="height:40px;"/></a>';
}

if(isset($_SESSION['access_token']))
{
    //add user to DB if logged in and not present in db
    addUser();
}

?>