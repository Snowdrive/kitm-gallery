<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-magic.php';

    $db = Database::connect();
    $googleID = $_SESSION['id'];
    $newGalleryName = $_POST['galleryname'];
    $galleryID = $_POST['galleryID'];
    $galleryOwner = $_POST['galleryOwner'];

    function editGalleryName(){
        global $db;
        global $googleID;
        global $galleryID;
        global $newGalleryName;
        global $galleryOwner;

        if($googleID === null){ // block if logged out
            header('location:../forbidden.php');
            return;
        }

        if($newGalleryName === null){ // block if no new name
            header('location:../forbidden.php');
            return;
        }
        if($galleryOwner !== $googleID){ // block if not owner
                    header('location:../forbidden.php');
                    return;
                }

        try{
            $updateName = "UPDATE galleries SET name = ? WHERE id = ?";
            $db->prepare($updateName)->execute([$newGalleryName, $galleryID]);
        }
        catch(PDOException $e){
            echo $updateName . "<br>" . $e->getMessage();
        }
    }

    editGalleryName();
    $_POST = array();
    header('location:../viewgallery.php?id='.$galleryID);
?>