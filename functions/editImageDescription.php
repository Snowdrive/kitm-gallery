<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-magic.php';

    $db = Database::connect();
    $googleID = $_SESSION['id'];
    $newImageDescription = $_POST['imagedescription'];
    $imageID = $_POST['imageID'];

    function editImageDescription(){
        global $db;
        global $googleID;
        global $imageID;
        global $newImageDescription;

        if($googleID === null){ // block if logged out
            header('location:../forbidden.php');
            return;
        }

        try{
            $updateDesc = "UPDATE images SET description = ? WHERE id = ?";
            $db->prepare($updateDesc)->execute([$newImageDescription, $imageID]);
        }
        catch(PDOException $e){
            echo $updateDesc . "<br>" . $e->getMessage();
            }
    }

    editImageDescription();
    header('location:../viewimage.php?id='.$imageID);
    $_POST = array();
?>