<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-magic.php';

    $target_dir = $_SERVER['DOCUMENT_ROOT'].'/uploads/';
    $target_file = $target_dir . time() . basename($_FILES["imageToUpload"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    if($_SESSION["id"] !== $_POST["galleryOwner"]){ //die if not gallery owner
        header('location:../forbidden.php');
        die();
    }

    if(!isset($_FILES)){ //die if no file
        header('location:../forbidden.php');
        die();
    }

    if($_POST["imagename"] == ""){ //die if no image name
        header('location:../forbidden.php');
        die();
    }

    $check = getimagesize($_FILES["imageToUpload"]["tmp_name"]); //check if file is image
    if($check == false) {
        header('location:../forbidden.php');
        die();
    }

    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") { //check if jpg or png
        header('location:../forbidden.php');
        die();
    }

    if ($_FILES["fileToUpload"]["size"] > 10000000) { //die if file over 10mb
        header('location:../forbidden.php');
        die();
    }

    $fileLocation = str_replace("/home/deimantas/kitm_public/","",$target_file);
    var_dump($fileLocation);

    move_uploaded_file($_FILES["imageToUpload"]["tmp_name"], $target_file);

    $db = Database::connect();

    try{
        $stmt = $db->prepare("INSERT INTO images (name, description, location, belongs_to) VALUES (?,?,?,?)");
        $stmt->execute([$_POST["imagename"], $_POST["imagedescription"], $fileLocation, $_POST["galleryID"]]);

    } catch(PDOException $e) {
        echo $e->getMessage();
    }

    $_FILES = array();
    header('location:../viewgallery.php?id='.$_POST["galleryID"]);
    $_POST = array();
?>