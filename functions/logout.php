<?php

include $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-config.php';

$google_client->revokeToken();

session_destroy();

header('location:../index.php');

?>
