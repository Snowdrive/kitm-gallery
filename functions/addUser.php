<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-magic.php';

    $db = Database::connect();
    $googleID = $_SESSION['id'];
    $user_displayname = $_SESSION['defaultDisplayName'];;
    $user_image = $_SESSION['image2'];
    
function addUser(){
    global $db;
    global $googleID;
    global $user_displayname;
    global $user_image;

    if($googleID === null){ // block from accessing if logged out
        return;
    }
    
    $stmt = $db->prepare("SELECT google_id from users WHERE google_id=?");
    $stmt->execute([$googleID]);

    $check = $stmt->fetch();

    if(!$check){
        // add profile if doesn't exist

        try{
            $insertUser = "INSERT INTO users (google_id, display_name, profile_image) VALUES (?,?,?)";
            $db->prepare($insertUser)->execute([$googleID, $user_displayname, $user_image]);
        }
        catch(PDOException $e){
            echo $insertUser . "<br>" . $e->getMessage();
        }
    } 
    else
    {
        return $check;
    }

}

?>