<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-magic.php';

    $db = Database::connect();
    $googleID = $_SESSION['id'];
    $newDisplayName = $_POST['displayname'];

    function editDisplayName(){
        global $db;
        global $googleID;
        global $newDisplayName;

        if($googleID === null){ // block from accessing if logged out
            header('location:../forbidden.php');
            return;
        }

        if($newDisplayName === null){ // block from adding if no new name
            header('location:../forbidden.php');
            return;
        }

        try{
            $updateName = "UPDATE users SET display_name = ? WHERE google_id = ?";
            $db->prepare($updateName)->execute([$newDisplayName, $googleID]);
        }
        catch(PDOException $e){
            echo $updateName . "<br>" . $e->getMessage();
        }
    }

    editDisplayName();
    $_POST = array();
    header('location:../myprofile.php');

?>