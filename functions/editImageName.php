<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-magic.php';

    $db = Database::connect();
    $googleID = $_SESSION['id'];
    $newImageName = $_POST['imagename'];
    $imageID = $_POST['imageID'];

    function editImageName(){
        global $db;
        global $googleID;
        global $imageID;
        global $newImageName;

        if($googleID === null){ // block if logged out
            header('location:../forbidden.php');
            return;
        }

        if($newImageName === null){ // block if no new name
            header('location:../forbidden.php');
            return;
        }

        try{
            $updateName = "UPDATE images SET name = ? WHERE id = ?";
            $db->prepare($updateName)->execute([$newImageName, $imageID]);
        }
        catch(PDOException $e){
            echo $updateName . "<br>" . $e->getMessage();
        }
        }

    editImageName();
    header('location:../viewimage.php?id='.$imageID);
    $_POST = array();
?>