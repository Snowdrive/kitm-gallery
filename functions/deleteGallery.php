<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-magic.php';

    $db = Database::connect();
    $googleID = $_SESSION['id'];
    $galleryID = $_POST['galleryID'];
    $galleryOwner = $_POST['galleryOwner'];

    function deleteGallery(){
        global $db;
        global $googleID;
        global $galleryID;
        global $galleryOwner;

        if($googleID === null){ // block if logged out
            header('location:../forbidden.php');
            return;
        }

        if($galleryOwner !== $googleID){ // block if not owner
                    header('location:../forbidden.php');
                    return;
                }

            var_dump($googleID);
            var_dump($galleryOwner);
            var_dump($galleryID);

        try{
            $delGal = "DELETE FROM galleries WHERE id = ?";
            $db->prepare($delGal)->execute([$galleryID]);
        }
        catch(PDOException $e){
            echo $delGal . "<br>" . $e->getMessage();
        }
    }

    deleteGallery();
    $_POST = array();
    header('location:../galleries.php');

?>