<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-magic.php';

    $db = Database::connect();
    $googleID = $_SESSION['id'];
    $imageID = $_POST['imageID'];
    $imageOwner = $_POST['imageOwner'];

    function deleteImage(){
        global $db;
        global $googleID;
        global $imageID;

        if($googleID === null){ // block if logged out
            header('location:../forbidden.php');
            return;
        }

        try{
            $delImg = "DELETE FROM images WHERE id = ?";
            $db->prepare($delImg)->execute([$imageID]);
        }
        catch(PDOException $e){
            echo $delImg . "<br>" . $e->getMessage();
            }
    }

    deleteImage();
    header('location:../viewgallery.php?id='.$imageOwner);
    $_POST = array();
?>