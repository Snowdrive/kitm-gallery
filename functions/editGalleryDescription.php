<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/functions/oauth-magic.php';

    $db = Database::connect();
    $googleID = $_SESSION['id'];
    $newGalleryDescription = $_POST['gallerydescription'];
    $galleryID = $_POST['galleryID'];
    $galleryOwner = $_POST['galleryOwner'];

    function editGalleryDescription(){
        global $db;
        global $googleID;
        global $galleryID;
        global $newGalleryDescription;
        global $galleryOwner;

        if($googleID === null){ // block if logged out
            header('location:../forbidden.php');
            return;
        }

        if($galleryOwner !== $googleID){ // block if not owner
                    header('location:../forbidden.php');
                    return;
                }

        try{
            $updateDesc = "UPDATE galleries SET description = ? WHERE id = ?";
            $db->prepare($updateDesc)->execute([$newGalleryDescription, $galleryID]);
        }
        catch(PDOException $e){
            echo $updateDesc . "<br>" . $e->getMessage();
        }
    }

    editGalleryDescription();
    $_POST = array();
    header('location:../viewgallery.php?id='.$galleryID);

?>