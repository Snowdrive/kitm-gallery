<?php include '_partials/header.view.php'; ?>
<?php include '_partials/bootstrap.include.php'; ?>
<?php include 'functions/getRandomGalleries.php'; //get some random galls ?>

<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>KITM Galerija</title>
<body>

    <div class="mt-2 mb-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center">
        <div class="h4">Panaršykite šias galerijas</div>
    </div>
    
    <div class="mt-2 mb-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center flex-wrap">
    <?php 
        foreach($randomGalleryInfo as $randomGalleryInfo1){
            echo '<div class="card ms-2 me-2 mt-1 mb-1 text-wrap" style="width: 18rem; height: 13rem;">';
            echo '<div class="card-body">';
            echo '<h5 class="card-title">'.$randomGalleryInfo1["name"].'</h5>';
            if ($randomGalleryInfo1["description"] == ""){
                echo '<p class="card-text text-wrap" style="height:4.5rem;">Aprašymas nepateikas.</p>';  
            } else {
                echo '<p class="card-text text-wrap overflow-hidden" style="height:4.5rem;">'.$randomGalleryInfo1["description"].'</p>'; 
            }
            echo '<a href="viewgallery.php?id='.$randomGalleryInfo1["id"].'" class="btn btn-primary mt-3 stretched-link">Naršyti</a>';
            echo '</div>';
            echo '</div>';    
        }
    ?>
    </div>

    <div class="mt-2 mb-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center">
        <a href="galleries.php" class="btn btn-primary ms-3 me-3" role="button">Pamatyti daugiau</a>
        <?php echo $login_button ?>
    </div>

    <?php include '_partials/footer.view.php'; ?>

</body>
</html>

