<?php include '_partials/header.view.php'; ?>
<?php include '_partials/bootstrap.include.php'; ?>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/functions/database.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Vartotojo Profilis</title>
</head>
<body>
    <?php 
        $profile_id = $_GET["id"];

        $db = Database::connect();
        try{
            $stmt=$db->prepare("SELECT google_id FROM users WHERE google_id = ?");
            $stmt->execute([$profile_id]);

            
        }
        catch(PDOException $e){
            echo $stmt . "<br>" . $e->getMessage();
        }
    
        $checkIfExists = $stmt->fetch();

        if($checkIfExists == false){  // if user ID doesn't exist, die
            header('location:../forbidden.php');
            die();
        }

        include 'functions/getPublicProfileDataFromDB.php'; //get public user data from DB, not google
    ?>

        <div class="d-flex flex-column justify-content-center align-items-center mt-3">
            <div class="mt-2">   
                <?php echo  '<img src="'.$profileImage["profile_image"].'" class="rounded-circle img-fluid"> '; ?>
            </div>
            <div class="mt-2">
                <?php echo  '<p class="h4">'.$displayName["display_name"].'</p>'?>
            </div>
            <div class="mt-2">
                <p class="h4">Galerijos: <?php echo $checkRows;?></p>
            </div>
        </div>

        <div class="mt-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center">
            <div class="h4">Vartotojo Galerijos</div>
        </div>

    <?php include 'functions/getUserGalleries.php'; //current users galleries ?>

    <div class="mt-2 mb-2 bg-light pt-3 pb-3 ms-3 me-3 rounded d-flex justify-content-center align-items-center flex-wrap">
    <?php 
        foreach($galleryInfo as $galleryInfo1){
            echo '<div class="card ms-2 me-2 mt-1 mb-1 text-wrap" style="width: 18rem; height: 13rem;">';
            echo '<div class="card-body">';
            echo '<h5 class="card-title">'.$galleryInfo1["name"].'</h5>';
            if ($galleryInfo1["description"] == ""){
                echo '<p class="card-text text-wrap" style="height:4.5rem;">Aprašymas nepateikas.</p>';  
            } else {
                echo '<p class="card-text text-wrap overflow-hidden" style="height:4.5rem;">'.$galleryInfo1["description"].'</p>'; 
            }
            echo '<a href="viewgallery.php?id='.$galleryInfo1["id"].'" class="btn btn-primary mt-3 stretched-link">Naršyti</a>';
            echo '</div>';
            echo '</div>';    
        }
    ?>
    </div>

    <?php include '_partials/footer.view.php'; ?>
    <?php $_GET = array(); //clear get ?> 


</body>